This is a collection of tools for writing astrophysics papers using LaTeX.  
It is principally geared toward submissions to ApJ and friends. The inludes a 
large bibTex database that can be continually updated and expanded.